<?php

namespace App\Controllers;

class Home extends BaseController{
    public function index($message=""){
        
        $data = [
            'title' => 'Home',
            'message' => $message
        ];
        
        $this->displayPage('pages/home',$data,1,1,0);
    }
}
