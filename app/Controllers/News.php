<?php

namespace App\Controllers;

use App\Models\NewsModel;
use App\Models\MenuModel;

class News extends BaseController{
    public function index(){
        $model = model(NewsModel::class);
        $menuModel = model(MenuModel::class);
        $data = [
            'news' => $model->getNews(),
            'menu' => $menuModel->getMenu(),
            'title' => 'News',
        ];
        $this->displayPage('news/overview',$data);

    }
    public function view($slug = false){
        $model = model(NewsModel::class);

        $data['news'] = $model->getNews($slug);

        if(empty($data['news'])){
            throw new \CodeIgniter\Exceptions\PageNotFoundException('Cannot find the news item: ' . $slug);
        }

        $data['title'] = $data['news']['title'];

        $this->displayPage('news/view',$data);

    }

    public function create(){
        $model = model(NewsModel::class);

        if($this->request->getMethod() === 'post' && $this->validate([
            'title' => 'required|min_length[3]|max_length[255]','body' => 'required'
        ])){
            $model->save([
                'title' => $this->request->getPost('title'),
                'slug' => url_title($this->request->getPost('title'), '-', true),
                'body' => $this->request->getPost('body'),
            ]);
            echo view('news/success');
        }
        else{
            echo view('templates/header', ['title' => 'Create a news item']);
            echo view('news/create');
            echo view('templates/footer');
        }
    }
}
?>