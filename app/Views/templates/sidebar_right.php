<style>
    .sidebarRight{
        background-color: grey;
        right:0;
        position: fixed;
        display: flex;
        flex-direction: column;
        height: 95%;
        width: 20%;
        text-align: center;
        vertical-align: middle;
    }
    .sidebar-item{
        font-size: 1.6em;
        color:white;
        border-bottom: 2px solid black;
        padding-bottom: 2px;
        text-decoration: none;
        width: 100%;
        height:max-content;
        background-color: rgba(128,0,0,1);
    }
</style>
<div class="sidebarRight">
    <div class=sidebar-item style='background-color:black;'>Playlists:</div>
    <a class='sidebar-item' href=''>Favorites</a>
</div>