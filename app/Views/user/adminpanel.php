<style>
    td{
        border:1px solid black;
        padding: 3px;
        font-size: 1.3em;
    }
    td:first-child{
        border-right:2px solid red;
        border-left:2px solid red;
    }
    table{
        border-collapse:collapse;
        border-spacing: 0;
        border: 2px solid black;
        margin:auto;
    }
    tr:first-child{
        font-weight: bold;
        border:2px solid green;
    }
    body{
        text-align: center;
    }
    a{
        text-decoration: none;
        color:red;
        width: 100%;
    }
    a:hover{
        animation: menu_item_color 1s infinite;

    }
    .linksubmit{
        border:0;
        background-color: rgba(0,0,0,0);
        color:red;
        font-size: 20.8px;
        margin:0;
        padding: 0;
        font-family: 'Times New Roman';
    }
    .linksubmit:hover{
        cursor: pointer;
    }
</style>

<table>

<tr>

    <?php $user = current($users); foreach($user as $key => $val): ?>
        
            <?php if($key != 'password') echo '<td>'.ucfirst(esc($key)).'</td>' ?>

    <?php endforeach; ?>
    <td>
        Actions
    </td>
</tr>

<?php foreach($users as $user): ?>
        <tr>
            <?php $id=0; ?>
            <?php foreach($user as $key => $val): ?>

                <?php if($key == 'id')$id = $val; ?>
                <?php if($key != 'password') echo '<td>'.esc($val).'</td>' ?>
                
            <?php endforeach; ?>
            <td>

                <a href='/adminpanel/<?= $id ?>'>Edit</a>

            </td>
        </tr>
<?php endforeach; ?>

</table>