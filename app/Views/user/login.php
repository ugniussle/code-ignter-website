<?php 
    $session = session();
    if($session->get('login') === true){
        
    }
?>
<form class='form' action="/login" method="post">
    <?= csrf_field() ?>
    <?= service('validation')->listErrors(); ?>

    <label class='form-key' for="username">Username</label>
    <input class='form-value' type="input" name="username"/><br />

    <label class='form-key' for="password">Password</label>
    <input class='form-value' type="password" name="password"/><br />

    <input class='form-value' type="submit" name="submit" value="Login!"><br />
</form>