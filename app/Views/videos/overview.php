<?php if(!empty($videos) && is_array($videos)): ?>

    <style>
        .videos-container{
            display: flex;
            flex-wrap: wrap;
            width: 100%;
        }
        .video-item{
            width: 32.2%;
            height: 30%;
            margin: 0.5%;
        }
        .video-item:hover{
            background-color: rgba(255,0,0,0.15);
        }
        .video-item>a{
            color:red;
            height: 100%;
            text-decoration: none;
        }
        .video-item>.video-link>img{
            height: auto;
            max-width: 100%;
        }
        .video-link{
            font-size: 1.3em;
        }
    </style>


    <div class='videos-container'>

    <?php foreach($videos as $video): ?>
        <div class="video-item">
            <a class='video-link' href='/videos/<?= $video['url']?>'>
                <img src='<?= base_url('Files/Thumbnails/'.$video['thumbnail']) ?>'>
            
                <?= esc($video['title']) ?><br>
            </a>
            By: <a href='<?= base_url('user/'.$video['uploader']) ?>'>
                <?= esc($video['uploader']) ?>
            </a><br>
            Views: <?= $video['views'] ?>
        </div>

    <?php endforeach; ?>

    </div>

<?php else: ?>

    <h3>No Videos</h3>

<?php endif ?>

