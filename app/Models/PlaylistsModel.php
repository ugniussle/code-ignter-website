<?php

namespace App\Models;

use CodeIgniter\Model;

class PlaylistsModel extends Model{
    protected $table = 'playlists';

    protected $allowedFields = ['url','title','user_id','visibility','views'];

    public function getVideos($url = false){
        if($url == false){
            return $this->findAll();
        }
        return $this->where(['url'=>$url])->first();
    }
    public function getUserVideos($user){
        if(($user ?? '') == ''){
            return [];
        }
        return $this->where(['uploader'=>$user])->findAll();
    }
    public function increaseViews($url){
        $row = $this->where(['url'=>$url])->first();
        $row['views']++;
        $this->update($row['id'],$row);
    }
}
?>