<?php
namespace App\Models;

use CodeIgniter\Model;

class adminModel extends Model{
    protected $table = 'admins';

    protected $allowedFields = ['user_id'];

    public function getInfo(){
        return $this->findAll();
    }

    public function isAdmin($id){
        $admins = $this->findAll();
        # $admin = $this->find($id); //use this

        foreach($admins as $admin)
            foreach($admin as $key => $value)
                if($key == 'user_id' && $value == $id)
                    return true;

        return false;
    }
}
?>