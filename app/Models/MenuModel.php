<?php

namespace App\Models;

use CodeIgniter\Model;

class menuModel extends Model{
    protected $table = 'menu';

    protected $allowedFields = ['name','link'];

    public function getMenu(){
        $data = $this->findAll();
        $session = session();
        if($session->get('login') == true) {
            $data = array_filter($data, function($v, $k) {
                return !($v['name'] == 'Login' || $v['name'] == 'Register' || $v['name'] == 'Admin Panel');
            },ARRAY_FILTER_USE_BOTH);

            $db = \Config\Database::connect();

            if($session->get('admin') == true){
                $data[]=['link' => 'adminpanel','name' => 'Admin Panel'];
            }
        }
        else {
            $data = array_filter($data,function($v, $k) {
                return !($v['name'] == 'Profile' || $v['name'] == 'Log out' || $v['name'] == 'Admin Panel' || $v['name'] == 'Create a video');
            },ARRAY_FILTER_USE_BOTH);
        }
        return $data;
    }
}
?>