<?php

namespace App\Models;

use CodeIgniter\Model;

class userModel extends Model{
    protected $table = 'users';

    protected $allowedFields = ['username','password','email'];

    public function getInfo(){
        return $this->findAll();
    }

    public function getUser($username){
        $result = $this->where(['username'=>$username])->first();
        log_message('debug', 'result type is '.gettype($result));
        return $result;
    }

    public function getUserName($id){
        $user = $this->find($id);
        return $user['username'];
    }

    public function getUserId($id){
        return $this->find($id);
    }
    
    public function deleteUser($id){
        if(!is_int($id)){
            echo "id is not an integer";
            exit();
        }

        return $this->delete($id);
    }
}
?>